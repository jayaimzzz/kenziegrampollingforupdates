const express = require("express");
const multer = require("multer");
const fs = require("fs");

const port = 3000;
const publicPath = ('public/')

const app = express();
app.use(express.json());
app.use(express.static(publicPath));
// app.set('views', './views');
app.set('view engine', 'pug');
const upload = multer({
    dest: publicPath + "uploads/"
});

const uploadedFiles = [];

app.post('/upload', upload.single('my-file'), function (request, response, next) {
    uploadedFiles.push(request.file.filename);
    response.render('uploaded', {
        picture: request.file.filename
    })
})

app.get('/', (req, res) => {
    const path = './public/uploads';
    fs.readdir(path, function (err, items) {
        items = items.filter(item => item !== ".DS_Store");
        items.sort((a, b) => fs.statSync(path + '/' + b).mtimeMs - fs.statSync(path + '/' + a).mtimeMs)
        items = items.map(item => item = {
            filename: item,
            uploadTimeStamp: fs.statSync(path + '/' + item).mtimeMs
        })
        res.render('index', {
            title: 'KenzieGram',
            pictures: items
        })
    })
})

app.post('/latest', (req, res) => {
    const path = './public/uploads'
    fs.readdir(path, function (err, items) {
        let newPictures = uploadedFiles.filter(picture => fs.statSync(path + '/' + picture).mtimeMs >= req.body.clientsLastUpdate);
        res.send({
            "newPictures": newPictures,
            "updateTimeStamp": Date.now(),
            "status": 200
        })
    })
})

app.listen(port);