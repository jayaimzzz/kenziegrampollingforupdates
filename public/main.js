const timeoutLength = 5000;
let timeStamp = Date.now();
let picturesDiv = document.getElementById('pictures');
let failedAttempts = 0;

const getNewPictures = async function () {
    let body = {
        'clientsLastUpdate': timeStamp
    }
    body = JSON.stringify(body)
    try {
        let res = await fetch('/latest', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: body
        });
        failedAttempts = 0;
        res = await res.json();
        timeStamp = res.updateTimeStamp
        if (res.newPictures.length > 0) {
            res.newPictures.forEach(picture => addPictureToPage(picture))
        }
        setTimeout(getNewPictures, timeoutLength);
    } catch (err) {
        failedAttempts++
        if (failedAttempts >= 2) {
            alert("Connection lost. Error message: " + err)
        } else { setTimeout(getNewPictures, timeoutLength);}
     
    }
};
setTimeout(getNewPictures, timeoutLength);

function addPictureToPage(path) {
    let imageDiv = document.createElement('div')
    imageDiv.classList = 'imageDiv';
    let imageElement = document.createElement('img');
    imageElement.classList = 'image';
    imageElement.src = 'uploads/' + path
    imageDiv.appendChild(imageElement);
    picturesDiv.insertBefore(imageDiv, picturesDiv.childNodes[0])
}